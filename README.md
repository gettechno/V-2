<p align="center"><img src="https://webincog.tk/img/logo.PNG" height="200">
</p>

<h1 align="center">GetTechno</h1>

Cool french website that I made for my school.
For a better experience, use this website on an iPad

https://webincog.tk

## Features

- PWA partially supported
- Now.gg bypass(PATCHED)
- HTML5 games
- Integrated proxy
- Url opener
- Theme changer

## Credits

- Caracal.js (Creator of Ultraviolet)
- Divide (Creator of TOMP)
- 3kh0 for most of the games
- Pix4rt for Logo and Trailer
- Shuttlegames for the Now.gg bypass

## Made by AldessSc
